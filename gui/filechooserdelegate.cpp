#include "filechooserdelegate.h"

using namespace Cutecat;

QWidget* FileChooserDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	// Initialize FileChooser inside _tree->viewport() for correct placement
	auto chooser = new FileChooser(FileChooser::Open, "", FileChooser::CompactStyle, parent);
	chooser->setGeometry(option.rect);
	return chooser;
}

void FileChooserDelegate::setEditorData(QWidget* editor, const QModelIndex &index) const
{
	auto chooser = (FileChooser*) editor;
	chooser->setFileName(index.data().toString());
}

void FileChooserDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	auto chooser = (FileChooser*) editor;
	model->setData(index, chooser->fileName());
}

QSize FileChooserDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	return QSize(0, 1.5 * option.font.pixelSize());
}
