#include "sidetabbar.h"

#include <QtGui/QPaintEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QStyleOptionTabV3>
#include <QtWidgets/QStylePainter>
#include <QtWidgets/QLabel>

using namespace Cutecat;

SideTabBar::SideTabBar(QWidget* parent) :
	QTabBar(parent)
{
}

QSize SideTabBar::tabSizeHint(int index) const
{
	int width = qApp->fontMetrics().width(tabText(index) + ' ') + iconSize().width();
	int height = qMax(qApp->fontMetrics().height(), iconSize().height()) + 10;
	return QSize(width, height);
}

void SideTabBar::paintEvent(QPaintEvent *event)
{
	// Initialize painter
	QStylePainter painter(this);
	
	for (int index=0; index<count(); index++)
	{
		QRect rect = tabRect(index);
		
		// Set tab drawing style with empty icon and text
		QStyleOptionTabV3 option;
		initStyleOption(&option, index);
		option.text = QString();
		option.icon = QIcon();
		option.documentMode = documentMode();
		
		// Draw the empty tab
		painter.drawControl(QStyle::CE_TabBarTab, option);
		
		// Draw the icon
		int iconLeft = rect.left() + QStyle::PM_TabBarTabHSpace/2;
		int iconTop = rect.top() + rect.height()/2 - iconSize().height()/2;
		painter.drawPixmap(iconLeft, iconTop, tabIcon(index).pixmap(iconSize()));
		
		// Draw the text
		QRect textRect = rect.adjusted(
			QStyle::PM_TabBarTabHSpace/2 + iconSize().width() + qApp->fontMetrics().width(' '),
			0, 0, 0);
		painter.drawText(textRect, Qt::AlignLeft | Qt::AlignVCenter, tabText(index));
	}
	
	// Finalize painter
	painter.end();
}
