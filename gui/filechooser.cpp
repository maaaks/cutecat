#include "filechooser.h"

#include <QtCore/QFileInfo>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QHBoxLayout>

using namespace Cutecat;

FileChooser::FileChooser(FileChooser::Behavior behavior, QString fileName, FileChooser::FileChooserStyle style, QWidget* parent) :
	QWidget(parent), _behavior(behavior)
{
	// Create text field and button
	_field = new QLineEdit(fileName);
	_button = new QToolButton();
	_button->setText("...");
	
	// Connect button with a slot on click
	connect(_button, SIGNAL(clicked()), this, SLOT(onClick()));
	
	// Place both widgets in a QHBoxLayout
	QHBoxLayout *layout = new QHBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(_field, 1);
	layout->addWidget(_button);
	setLayout(layout);
	
	// When activating FileChooser, activate the text field
	setFocusProxy(_field);
	
	// Set style
	setFileChooserStyle(style);
}

FileChooser::FileChooser(Behavior behavior, QString fileName, QWidget *parent) :
	FileChooser(behavior, fileName, NormalStyle, parent)
{
}

QString FileChooser::fileName()
{
	return _field->text();
}

void FileChooser::setFileName(QString fileName)
{
	_field->setText(fileName);
	emit fileNameChanged(fileName);
}

FileChooser::FileChooserStyle FileChooser::style()
{
	return _field->hasFrame() ? NormalStyle : CompactStyle;
}

void FileChooser::setFileChooserStyle(FileChooserStyle style)
{
	if (style == NormalStyle) {
		_field->setFrame(true);
		_button->setAutoFillBackground(false);
		layout()->setSpacing(5);
	} else {
		_field->setFrame(false);
		_button->setAutoFillBackground(true);
		layout()->setSpacing(0);
	}
}

void FileChooser::clear()
{
	_field->clear();
}

void FileChooser::onClick()
{
	if (_behavior == Open)
	{
		QString fileName = QFileDialog::getOpenFileName(
			0, QString(),
			QDir(_field->text()).absolutePath()
		);
		
		if (fileName.length() > 0)
			setFileName(fileName);
	}
	else
	{
		QString fileName = QFileDialog::getSaveFileName(
			0, QString(),
			QDir(_field->text()).absolutePath()
		);
		
		if (fileName.length() > 0)
			setFileName(fileName);
	}
}
