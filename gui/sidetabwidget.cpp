#include "sidetabwidget.h"

using namespace Cutecat;

SideTabWidget::SideTabWidget(QWidget* parent) :
	QTabWidget(parent)
{
	setTabBar(new SideTabBar);
}

SideTabWidget* SideTabWidget::convert(QTabWidget*const old)
{
	auto newWidget = new SideTabWidget(old->parentWidget());
	newWidget->setDocumentMode(old->documentMode());
	newWidget->setTabPosition(old->tabPosition());
	
	QList<QWidget*> widgets;
	QList<QIcon> icons;
	QList<QString> texts;
	QList<QString> toolTips;
	QList<QString> whatsThises;
	
	int count = old->count();
	for (int i=0; i<old->count(); i++) {
		widgets     << old->widget(i);
		icons       << old->tabIcon(i);
		texts       << old->tabText(i);
		toolTips    << old->tabToolTip(i);
		whatsThises << old->tabWhatsThis(i);
	}
	
	for (int i=0; i<count; i++) {
		newWidget->addTab(widgets[i], icons[i], texts[i]);
		newWidget->setTabToolTip(i, toolTips[i]);
		newWidget->setTabWhatsThis(i, whatsThises[i]);
	}
	
	return newWidget;
}
