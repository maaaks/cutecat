#ifndef CUTECAT_SIDETABBAR_H
#define CUTECAT_SIDETABBAR_H

#include <QtWidgets/QTabBar>

namespace Cutecat
{
	class SideTabBar : public QTabBar
	{
		Q_OBJECT
		
	public:
		SideTabBar(QWidget *parent = 0);
		
	protected:
		virtual QSize tabSizeHint(int index) const;
		void paintEvent(QPaintEvent *event);
	};
}

#endif // CUTECAT_SIDETABBAR_H
