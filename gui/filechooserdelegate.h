#ifndef CUTECAT_FILECHOOSER_DELEGATE_H
#define CUTECAT_FILECHOOSER_DELEGATE_H

#include <QtWidgets/QItemDelegate>
#include "../gui/filechooser.h"

namespace Cutecat
{
	class FileChooserDelegate : public QItemDelegate
	{
	public:
		virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
		virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
		virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
	};
}

#endif // CUTECAT_FILECHOOSER_DELEGATE_H
