#ifndef CUTECAT_SIDETABWIDGET_H
#define CUTECAT_SIDETABWIDGET_H

#include <QtWidgets/QTabWidget>
#include "sidetabbar.h"

namespace Cutecat
{
	class SideTabWidget : public QTabWidget
	{
		Q_OBJECT
		
	public:
		SideTabWidget(QWidget *parent=0);
		
		static SideTabWidget* convert(QTabWidget*const old);
	};
}

#endif // CUTECAT_SIDETABWIDGET_H
