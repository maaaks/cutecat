#ifndef CUTECAT_FILECHOOSER_H
#define CUTECAT_FILECHOOSER_H

#include <QtWidgets/QToolButton>
#include <QtWidgets/QLineEdit>

namespace Cutecat {
class FileChooser : public QWidget
{
	Q_OBJECT
	
public:
	enum Behavior {Open, Save};
	enum FileChooserStyle {NormalStyle, CompactStyle};
	
private:
	QToolButton *_button;
	QLineEdit *_field;
	Behavior _behavior;
	FileChooserStyle _style;

public:
	FileChooser(Behavior behavior=Open, QString fileName="", FileChooserStyle style=NormalStyle, QWidget* parent=0);
	FileChooser(Behavior behavior, QString fileName, QWidget* parent);
	
	QString fileName();
	void setFileName(QString fileName);
	
	FileChooserStyle style();
	void setFileChooserStyle(FileChooserStyle style);
	
public slots:
	void clear();
	
private slots:
	void onClick();
	
signals:
	void fileNameChanged(const QString& fileName);
};
}

#endif // CUTECAT_FILECHOOSER_H
