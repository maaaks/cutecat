#include <QtWidgets/QApplication>

#include "demo/cutecatdemo.h"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Cutecat::CutecatDemo window;
	window.show();
	app.exec();
}
