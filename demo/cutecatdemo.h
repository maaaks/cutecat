#ifndef CUTECATDEMO_H
#define CUTECATDEMO_H

#include <QtWidgets/QWidget>

namespace Cutecat {
class CutecatDemo : public QWidget
{
	Q_OBJECT

public:
	CutecatDemo();
	virtual ~CutecatDemo();
};
}

#endif // CUTECATDEMO_H
