#include "cutecatdemo.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

#include "gui/filechooser.h"

using namespace Cutecat;

CutecatDemo::CutecatDemo()
{
	// Resize
	resize(600, 400);
	
	// Move to screen's center
	QRect rect = geometry();
	rect.moveCenter(qApp->desktop()->availableGeometry(this).center());
	move(rect.topLeft());
	
	// Initialize vertical box layout
	auto layout = new QVBoxLayout;
	setLayout(layout);
	
	
	/**************************************************************
	 * FileChooser demos
	 *************************************************************/
	
	layout->addWidget(new QLabel("Cutecat::FileChooser with Open behavior:"));
	layout->addWidget(new FileChooser(FileChooser::Open, "", this));
	
	layout->addWidget(new QLabel("Cutecat::FileChooser with Save behavior:"));
	layout->addWidget(new FileChooser(FileChooser::Save, "", this));
	
	layout->addWidget(new QLabel("Compact Cutecat::FileChooser with Open behavior:"));
	layout->addWidget(new FileChooser(FileChooser::Open, "", FileChooser::CompactStyle, this));
	
	layout->addWidget(new QLabel("Compact Cutecat::FileChooser with Save behavior:"));
	layout->addWidget(new FileChooser(FileChooser::Save, "", FileChooser::CompactStyle, this));
	
	
	// Add a stretch at the end of the window
	layout->addStretch();
}

CutecatDemo::~CutecatDemo()
{
}

