#include <QtCore/QString>
#include <QtCore/QTextStream>


/** Provides a quick Qt-compatible way to say something to console.
 * 
 * Everything that you pass to the macro is appended to an empty QString.
 */
#define OUT(string) \
	QTextStream(stdout) << (QString() + string) << "\n"


/** Similar to OUT(), but exits the program after writing to console.
 */
#define DIE(string) \
	[](){ QTextStream(stdout) << (QString() + string) << "\n"; exit(0); }()


/** Creates QApplication and immediately sets its name and version.
 * 
 * Must be used when \c argc and \c argv are available.
 */
#define CREATE_APP(objName, applicationName, applicationVersion) \
	QApplication objName(argc, argv); \
	objName.setApplicationName(applicationName); \
	objName.setApplicationVersion(applicationVersion);


/** The same as CREATE_APP but creates QCoreApplication.
 */
#define CREATE_COREAPP(objName, applicationName, applicationVersion) \
	QCoreApplication objName(argc, argv); \
	objName.setApplicationName(applicationName); \
	objName.setApplicationVersion(applicationVersion);


/** Simple structure similar to QPair but probiding custom names for «first» and «second».
 * 
 * Sample usage:
 * \code
 * NAMED_PAIR(KeyAndValue, QString, coolKey, QVariant, coolValue);
 * KeyAndValue kv;
 * kv.coolKey = "Foo";
 * kv.coolValue = "Bar";
 * \endcode
 * 
 * The code is, in most, copied from original QPair structure.
 * 
 * Seems to work but may cause some strange parsing behaviour in KDevelop. :-(
 */
#define NAMED_PAIR(className, T1, firstName, T2, secondName) \
	struct className { \
		typedef T1 firstName##_type; \
		typedef T2 secondName##_type; \
		className() : firstName(), secondName() {} \
		className(const T1 &t1, const T2 &t2) : firstName(t1), secondName(t2) {} \
		T1 firstName; \
		T2 secondName; \
	};
