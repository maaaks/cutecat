#ifndef CUTECAT_CUTESETTINGS_H
#define CUTECAT_CUTESETTINGS_H

#include <QtCore/QSettings>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

namespace Cutecat {

/** This class extends functionality of QSettings in some ways.
 * 
 * These are features that are added:
 *  @li support for empty fileName in order to work with settings before they will be saved: see constructors
 *  @li ability to set and get whole groups of keys and values with QMap: see importGroup(), exportGroup()
 *  @li methods for working with comma-separated lists of strings: see getValues() and setValues()
 *  @li method for copying all settings into a new object: see copy()
 * 
 * QSettings' standard features should work normally, so you can easily replace QSettings with CuteSettings in your code.
 * 
 * @NOTE The class can be used in C++11 only since it uses constructor inheritance.
 * If you want to complicate the code in order to backport it to C++03,
 * feel free to use my code mentioning Maaaks.ru in any place somebody can read.
 * (I think you'll need to follow Qt's source license too if you copy QSettings constructor implementation.)
 */
class CuteSettings : public QSettings
{
	Q_OBJECT
	
	bool isSaved = true;
	
private:
	/** Aux function which returns temporary file for using in constructor.
	 */
	static QString tempFileName(QString fileName);
	
public:
	// Most constructors declarations are just copied from the QSettings.
	CuteSettings(const QString &organization, const QString &application="", QObject *parent=0) :
		QSettings(organization, application, parent) {}
	CuteSettings(Scope scope, const QString &organization, const QString &application="", QObject *parent=0) :
		QSettings(scope, organization, application, parent) {}
	CuteSettings(Format format, Scope scope, const QString &organization, const QString &application="", QObject *parent=0) :
		QSettings(format, scope, organization, application, parent) {}
    CuteSettings(QObject *parent=0) :
		QSettings(parent) {}
	
	/** Initializes object with the given file or a temporary file.
	 * 
	 * This is the only QSettings' constructor that is overriden in CuteSettings.
	 * 
	 * When no \c fileName given, it creates a temporary file and uses it to initialize the object.
	 * Later, you always can save it to another file using copy() method.
	 * This is useful when you want to make software which can work with unsaved files and then save it.
	 * 
	 * @NOTE The format of settings initialized using empty \c fileName is always QSettings::IniFormat.
	 */
    CuteSettings(const QString &fileName, Format format, QObject *parent=0) :
		QSettings(tempFileName(fileName), format, parent), // Replace empty filename with a temporary filename
		isSaved(!fileName.isEmpty())                       // Set isSaved based on original fileName
	{}
	
	/** Return current group as QMap.
	 * 
	 * Every item in the QMap can be either a QVariant representing one value
	 * or another QMap containing subgroup and its subsubgroups, recursively.
	 * 
	 * If you want to get QMap with all values in QSettings, call exportGroup() on the top level.
	 */
	QMap<QString,QVariant> exportGroup(int levels=-1);
	
	/** Fill the current group with values from QMap. All existing values will be deleted.
	 */
	void importGroup(QMap<QString,QVariant> data);
	
	/** Gets value as a string containing comma-separated strings.
	 */
	QStringList values(QString key, QStringList defaultValue = QStringList());
	
	/** Overrides standard QSettings::setValue() and adds support for QStringList.
	 * 
	 * When a QStringList is given in \c value,
	 * it is saved as a string containing comma-separated values.
	 */
	void setValue(QString key, QVariant value);
	
	/** Copies current settings to a new object «linked» with given \c fileName.
	 * 
	 * The resulting object will contain the same data as the original but will be pointed to another file.
	 * Internally, exportGroup() and importGroup() are used to copy current values.
	 * 
	 * When no \c fileName given, a temporary *.ini file will be created.
	 * Else, the new copy will be created using the given fileName.
	 * Note that previous values stored in given file (if it existed) aren't imported to the copy.
	 */
	CuteSettings* copy(QString fileName="", QSettings::Format format=QSettings::IniFormat);
	
	/** Returns fileName for a saved object and empty string for an unsaved.
	 * 
	 * To make this difference, \c isSaved field is added to this class.
	 */
	QString fileName();
};
}

#endif // CUTECAT_CUTESETTINGS_H
