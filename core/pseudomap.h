#ifndef CUTECAT_PSEUDOMAP_H
#define CUTECAT_PSEUDOMAP_H

#include <QtCore/QList>
#include <QtCore/QString>

namespace Cutecat {
/** Template QList-based class which also provides basic QMap-like features.
 * 
 * Internally, a PseudoMap is simply a QList of \c T objects or structs.
 * Every \c T object should have a key (of class \c Key) which will be used for search.
 * PseudoMap is an abstract class. In order to use it,
 * you must implement \c itemKey() method which should extract \c Key from a \c T.
 * 
 * For example, if you want to have a PseudoMap of devices indexes by model id,
 * your code may look like this:
 * \code
 * struct Device {
 * 	QString modelId;
 * 	QDateTime productionDate;
 * 	QString description;
 * };
 * 
 * class DevicesPseudoMap : public PseudoMap<QString,Device> {
 * 	virtual QString itemKey(const Device& item) const {
 * 		return item.modelId;
 * 	}
 * };
 * \endcode
 * 
 * The class defined above can be accessed either by \c int (as \c QList<Device>)
 * or by \c QString (as \c QMap<QString,Device>).
 */
template <class Key, class T>
class PseudoMap : public QList<T>
{
public:
	T& operator[](int index)
	{
		return QList<T>::operator[](index);
	}
	const T& operator[](int index) const
	{
		return QList<T>::operator[](index);
	}
	
	T& operator[](Key key)
	{
		int count = this->count();
		
		for (int i=0; i<count; i++)
			if (itemKey(QList<T>::operator[](i)) == key)
				return QList<T>::operator[](i);

		QList<T>::append(T());
		return QList<T>::operator[](count);
	}
	const T& operator[](Key key) const
	{
		int count = this->count();
		
		for (int i=0; i<count; i++)
			if (itemKey(QList<T>::operator[](i)) == key)
				return QList<T>::operator[](i);

		return T();
	}
	
	const T& at(int index) const
	{
		return QList<T>::at(index);
	}
	
	const T& at(Key key) const
	{
		int count = this->count();
		
		for (int i=0; i<count; i++)
			if (itemKey(QList<T>::at(i)) == key)
				return QList<T>::at(i);
		return QList<T>::at(count);
	}
	
	bool contains(const Key key) const
	{
		int count = this->count();
		for (int i=0; i<count; i++)
			if (itemKey(QList<T>::operator[](i)) == key)
				return true;
		return false;
	}

	QList<Key> keys() const
	{
		QList<Key> keys;
		
		int count = this->count();
		for (int i=0; i<count; i++)
			keys << itemKey(this->at(i));
		return keys;
	}
	
	bool operator==(const PseudoMap<Key,T>& other) const
	{
		if (this->count() != other.count())
			return false;
		
		foreach (Key key, keys())
			if (this->at(key) != other.at(key))
				return false;
		
		return true;
	}
	
	bool operator!=(const PseudoMap<Key,T>& other) const {
		return !operator==(other);
	}
	
	virtual Key itemKey(const T& item) const = 0;
};
}

#endif // CUTECAT_PSEUDOMAP_H
