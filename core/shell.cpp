#include "shell.h"

#include <QtCore/QDir>
#include <QtCore/QProcess>
#include <QtCore/QRegularExpression>
#include <QtCore/QTemporaryFile>
#include <QtCore/QTextStream>
#include <fcntl.h>
#include <stdio.h>

using namespace Cutecat;

int Shell::shell(QString command, QString directory)
{
	// If directory given, go there
	QString oldDirectory = QDir::currentPath();
	if (!directory.isNull())
		QDir::setCurrent(directory);
	
	// Execute the "sh" process with given command
	int result = QProcess::execute("sh", {"-c", command});
	
	// If directory was given, go back
	if (!directory.isEmpty())
		QDir::setCurrent(oldDirectory);
	
	// Return the result
	return result;
}

int Shell::runScript(QString script, const QStringList& arguments, const QString& directory, const QProcessEnvironment& env)
{
	// Add default shebang if there is no one
	if (!script.startsWith("#!"))
		script.prepend("#!/bin/sh\n");
	
	// Create temporary file
	// (Do it in a scope to avoid strange file locks by QTemporaryFile.)
	QString fileName;
	{
		QTemporaryFile file(QDir::tempPath() + "/Cutecat.Shell.XXXXXX");
		file.open();
		QTextStream(&file) << script;
		file.setAutoRemove(false);
		file.close();
		fileName = file.fileName();
	}
	
	// Make the temporary file executable
	QFile(fileName).setPermissions(QFile::ReadUser|QFile::ExeUser);
	
	// Run the script
	QProcess process;
	process.setWorkingDirectory(directory);
	process.setProcessEnvironment(env);
	process.setProcessChannelMode(QProcess::ForwardedChannels);
	process.start(fileName, arguments);
	
	// Wait for the interpreter to exit
	process.waitForFinished(-1);
	
	// Return the result
	return process.exitCode();
}

int Shell::runScript(QString script, const QProcessEnvironment& env)
{
	return runScript(script, {}, QString(), env);
}

void Shell::forceCopy(QString source, QString destination, bool mkdir, bool dereference, bool recursive)
{
	if (mkdir) {
		QDir(destination).mkpath("..");
	}
	
	QStringList arguments;
	if (dereference) arguments << "--dereference";
	if (recursive)   arguments << "--recursive";
	arguments << source;
	arguments << destination;
	QProcess::execute("cp", arguments);
}

Shell::Style::Style(Shell::Fg foreground, Shell::Bg background, Shell::Font font) :
	foreground(foreground), background(background), font(font)
{
}

void Shell::print(QString text, Shell::Fg foreground, Shell::Bg background, Shell::Font font, bool newline)
{
	// Define three aux maps that store fg, bg amd style numbers as strings
	
	static QMap<Fg,QString> strFg;
	if (strFg.isEmpty()) {
		strFg[Fg::Black]  = "30";
		strFg[Fg::Red]    = "31";
		strFg[Fg::Green]  = "32";
		strFg[Fg::Yellow] = "33";
		strFg[Fg::Blue]   = "34";
		strFg[Fg::Purple] = "35";
		strFg[Fg::Cyan]   = "36";
		strFg[Fg::White]  = "37";
	}
	
	static QMap<Bg,QString> strBg;
	if (strBg.isEmpty()) {
		strBg[Bg::Black]  = "40";
		strBg[Bg::Red]    = "41";
		strBg[Bg::Green]  = "42";
		strBg[Bg::Yellow] = "43";
		strBg[Bg::Blue]   = "44";
		strBg[Bg::Purple] = "45";
		strBg[Bg::Cyan]   = "46";
		strBg[Bg::White]  = "47";
	}
	
	static QMap<Font,QString> strFont;
	if (strFont.isEmpty()) {
		strFont[Font::Normal]    = "0";
		strFont[Font::Bold]      = "1";
		strFont[Font::Underline] = "4";
	}
	
	// Construct "\e[...m" prefix for text
	QString open;
	if (font != Font::NoChange && foreground != Fg::NoChange && background != Bg::NoChange)
		open = "\e[" + strFont[font] + ";" + strFg[foreground] + ";" + strBg[background] + "m";
	else if (font != Font::NoChange && foreground != Fg::NoChange)
		open = "\e[" + strFont[font] + ";" + strFg[foreground] + "m";
	else if (font != Font::NoChange && background != Bg::NoChange)
		open = "\e[" + strFont[font] + ";" + strBg[background] + "m";
	else if (foreground != Fg::NoChange && background != Bg::NoChange)
		open = "\e[" + strFg[foreground] + ";" + strBg[background] + "m";
	else if (foreground != Fg::NoChange)
		open = "\e[" + strFg[foreground] + "m";
	else if (background != Bg::NoChange)
		open = "\e[" + strBg[background] + "m";
	
	// If prefix is not empty, construct suffix that will reset formatting
	QString close = open.isEmpty() ? "" : "\e[0m";
	
	// Print!
	QTextStream(stdout) << open << text << close << (newline?"\n":"");
}

void Shell::print(QString text, Shell::Style style)
{
	print(text, style.foreground, style.background, style.font);
}

void Shell::println(QString text, Shell::Fg foreground, Shell::Bg background, Shell::Font font)
{
	print(text, foreground, background, font, true);
}

void Shell::println(QString text, Shell::Style style)
{
	print(text, style.foreground, style.background, style.font, true);
}

QString Shell::expand(QString string, const QProcessEnvironment& env)
{
	QRegularExpression re("\\$(\\w+)|\\$\\{(\\w+)\\}"); // Matches $FOO and ${FOO}
	auto iterator = re.globalMatch(string);
	
	/* This delta will change while we will replace matches.
	 * 
	 * Example 1. After changing «$foo bar» to «1234 bar», delta doesn't change.
	 * Example 2. After changing «$foo bar» to «12345 bar», delta increases by 1
	 * 		(«bar» moves right by 1 character).
	 * Example 3. After changing «$foo bar» to «12 bar», delta decreases by 2
	 * 		(«bar» moves left by 2 characters).
	 */
	int delta = 0;
	
	while (iterator.hasNext()) {
		auto match = iterator.next();
		QString key = !match.captured(1).isNull() ? match.captured(1) : match.captured(2);
		QString value = env.value(key);
		
		string = string.replace(match.capturedStart()+delta, match.capturedLength()+delta, value);
		delta += value.length() - match.capturedLength();
	}
	
	return string;
}


