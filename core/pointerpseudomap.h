#ifndef CUTECAT_POINTERPSEUDOMAP_H
#define CUTECAT_POINTERPSEUDOMAP_H

#define list QList<T*>

#include <QtCore/QList>
#include <QtCore/QString>

namespace Cutecat {
template <class Key, class T>
class PointerPseudoMap : public QList<T*>
{
public:
	virtual Key itemKey(const T& item) const = 0;
	
	T& operator[](int index) {
		return *list::operator[](index);
	}
	const T& operator[](int index) const {
		return *list::operator[](index);
	}
	
	T& operator[](Key key) {
		for (int i=0; i<this->count(); i++)
			if (itemKey(*list::operator[](i)) == key)
				return *list::operator[](i);

		list::append(new T);
		return *list::last();
	}
	const T& operator[](const Key& key) const {
		for (int i=0; i<this->count(); i++)
			if (itemKey(*list::operator[](i)) == key)
				return *list::operator[](i);
	}
	
	const T& at(int index) const {
		return *list::at(index);
	}
	const T& at(const Key& key) const {
		for (int i=0; i<this->count(); i++)
			if (itemKey(*list::at(i)) == key)
				return *list::at(i);
		return T();
	}
	
	bool contains(const Key& key) const {
		int count = this->count();
		for (int i=0; i<count; i++)
			if (itemKey(*list::operator[](i)) == key)
				return true;
		return false;
	}

//	QList<Key> keys() const {
//		QList<Key> keys;
		
//		for (int i=0; i<this->count(); i++)
//			keys << itemKey(this->at(i));
//		return keys;
//	}
	
//	bool operator==(const PointerPseudoMap<Key,T>& other) const {
//		if (this->count() != other.count())
//			return false;
		
//		for (int i=0; i<this->count(); i++)
//			if (itemKey(this->at(i)) != other.at(i))
//				return false;
//		return true;
//	}
//	bool operator!=(const PointerPseudoMap<Key,T>& other) const {
//		return !operator==(other);
//	}
//	PointerPseudoMap<Key,T>& operator<<(const PointerPseudoMap& other) {
//		for (int i=0; i<other.count(); i++)
//			*this << other.list::operator[](i);
//		return *this;
//	}
//	PointerPseudoMap<Key,T>& operator<<(T* item) {
//		list::operator<<(item);
//		return *this;
//	}
	
	struct const_iterator {
		const PointerPseudoMap& map;
		int index;
		
		const_iterator(const PointerPseudoMap& map, int index) : map(map), index(index) {}
		const T& operator*() { return map[index]; }
		bool operator!=(const const_iterator& other) { return index!=other.index; }
		const const_iterator& operator++() { index++; return *this; }
	};
	const_iterator begin() const { return (this->count()>0) ? const_iterator(*this, 0) : end(); }
	const_iterator end() const   { return const_iterator(*this, this->count()); }
};
}

#undef list
#endif // CUTECAT_POINTERPSEUDOMAP_H
