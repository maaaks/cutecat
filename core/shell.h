#ifndef CUTECAT_SHELL_H
#define CUTECAT_SHELL_H

#include <QtCore/QDir>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QString>
#include <QtCore/QStringList>

namespace Cutecat {

/** Shell provides set of static methods for executing various shell commands.
 * 
 * All of them are simply syntax-sugared wrappers for shell commands.
 * 
 * @note All these methods may be Unix-specific.
 */
class Shell
{
public:
	enum class Fg {
		NoChange = -1,
		Black  = 30,
		Red    = 31,
		Green  = 32,
		Yellow = 33,
		Blue   = 34,
		Purple = 35,
		Cyan   = 36,
		White  = 37,
	};
	
	enum class Bg {
		NoChange = -1,
		Black  = 40,
		Red    = 41,
		Green  = 42,
		Yellow = 43,
		Blue   = 44,
		Purple = 45,
		Cyan   = 46,
		White  = 47,
	};
	
	enum class Font {
		NoChange = -1,
		Normal    = 0,
		Bold      = 1,
		Underline = 4,
	};
	
	struct Style {
		Fg foreground = Fg::NoChange;
		Bg background = Bg::NoChange;
		Font font = Font::NoChange;
	public:
		Style(Fg foreground, Bg background, Font font);
	};
	
	/** Executes \c command as a console script line, opyionally in given \c directory.
	 * 
	 * Internally, it calls \c sh with \c -c option. This allows using pipes, \c 2>/dev/null, etc.
	 * 
	 * Example:
	 * \code Shell::shell("cat Packages | gzip  --stdout --best > Packages.gz") \endcode
	 */
	static int shell(QString command, QString directory=QString());
	
	/** Executes \c script (given as source text).
	 * 
	 * You can define the interpreter in the shebang line, else \c sh will be used.
	 *
	 * Examples:
	 * \code Shell::runScript("echo hello"); \endcode
	 * \code Shell::runScript("#!/usr/bin/python\n print('hello')"); \endcode
	 */
	static int runScript(QString script, const QStringList& arguments=QStringList(), const QString& directory=QString(), const QProcessEnvironment& env=QProcessEnvironment::systemEnvironment());
	static int runScript(QString script, const QProcessEnvironment& env);
	
	/** Executes \c cp command with some arguments that mean «copy it anyway».
	 * 
	 * Note that, by default, \c cp overwrites destination file,
	 * and this behavior currently cannot be changed with this function.
	 * 
	 * All the arguments can be changed, but the defaults are \c true.
	 * 
	 * @param mkdir Create parent directory if it doesn't exist.
	 * @param dereference Follow symlinks to find the real source file.
	 * 		Corresponds to \c --dereference option.
	 * @param recursive Copy \c source even if it is a directory.
	 * 		Corresponds to \c --recursive option.
	 */
	static void forceCopy(QString source, QString destination,
		bool mkdir=true, bool dereference=true, bool recursive=true);
	
	static void print(QString text, Fg foreground=Fg::NoChange, Bg background=Bg::NoChange, Font font=Font::NoChange, bool newline=false);
	static void print(QString text, Style style);
	static void println(QString text, Fg foreground=Fg::NoChange, Bg background=Bg::NoChange, Font font=Font::NoChange);
	static void println(QString text, Style style);
	
	/**
	 * Replaces \c $FOO and \c ${FOO} entries in \c string with value of \c FOO variable in \c env.
	 * Empty or unexistent variables are replaced with empty substring.
	 */
	static QString expand(QString string, const QProcessEnvironment& env=QProcessEnvironment::systemEnvironment());
};
}

#endif // CUTECAT_SHELL_H
