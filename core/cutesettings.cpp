#include "cutesettings.h"

#include <QtCore/QStringList>
#include <QtCore/QTemporaryFile>

using namespace Cutecat;

QMap<QString,QVariant> CuteSettings::exportGroup(int levels)
{
	QMap<QString,QVariant> result;
	
	// Collect all keys here
	foreach (QString key, childKeys())
		result[key] = value(key);
	
	// If there are "unused" levels, collect all subgroups recursively
	if (levels != -1 && levels > 0)
		foreach (QString key, childGroups())
		{
			// Go one level down
			beginGroup(key);
			
			// Call the same method here
			result[key] = exportGroup(levels-1);
			
			// Go one level up
			endGroup();
		}
	else
		foreach (QString key, allKeys())
			result[key] = value(key);
	
	// Return result :-)
	return result;
}

void CuteSettings::importGroup(QMap<QString, QVariant> data)
{
	// Delete all keys and groups on the current level
	foreach (QString key, allKeys())
		remove(key);
	
	// Import keys and groups from QMap
	foreach (QString key, data.keys())
		if (data[key].type() != QVariant::Map) {
			// If there's a value, set it
			setValue(key, data[key]);
		} else {
			// If there's a subgroup, call the same method one level lower
			beginGroup(key);
			importGroup(data[key].toMap());
			endGroup();
		}
}

QStringList CuteSettings::values(QString key, QStringList defaultValue)
{
	QStringList rawList = value(key, defaultValue).toString().split(",");
	QStringList result;
	foreach (QString rawPart, rawList)
		result << rawPart.trimmed();
	return result;
}

void CuteSettings::setValue(QString key, QVariant value)
{
	if (value.type() == QVariant::StringList)
		QSettings::setValue(key, value.toStringList().join(", "));
	else
		QSettings::setValue(key, value);
}

CuteSettings* CuteSettings::copy(QString fileName, QSettings::Format format)
{
	CuteSettings* copy = fileName.isEmpty()
		? new CuteSettings
		: new CuteSettings(fileName, format);
	
	copy->importGroup(this->exportGroup());
	copy->sync();
	return copy;
}

QString CuteSettings::tempFileName(QString fileName)
{
	// If fileName is not empty, return it as is
	if (!fileName.isEmpty())
		return fileName;
	
	// Create temporary file
	QTemporaryFile file("cutesettings.XXXXXX");
	
	// Open and close it to get a valid filename
	file.open();
	file.close();
	
	// Finally, return its name
	return file.fileName();
}

QString CuteSettings::fileName()
{
	if (isSaved)
		return QSettings::fileName();
	else
		return "";
}
