#ifndef CUTECAT_MAP_H
#define CUTECAT_MAP_H

#include <QtCore/QMap>

namespace Cutecat
{

/** A QMap with additional non-operators get() and set().
 */
template <class Key, class T>
class Map : public QMap<Key,T>
{
public:
	Map() : QMap<Key,T>() {};
	Map(const QMap<Key,T> &other) : QMap<Key,T>(other) {};
	
	T get(Key key) { return (*this)[key]; }
	void set(Key key, T value) { (*this)[key] = value; }
};
}

#endif // CUTECAT_MAP_H
